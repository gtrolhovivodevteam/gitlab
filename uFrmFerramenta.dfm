object frmFerramenta: TfrmFerramenta
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Ferramenta'
  ClientHeight = 148
  ClientWidth = 352
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 13
    Width = 37
    Height = 13
    Caption = 'C'#243'digo:'
  end
  object Label2: TLabel
    Left = 8
    Top = 61
    Width = 31
    Height = 13
    Caption = 'Nome:'
  end
  object Edit1: TEdit
    Left = 8
    Top = 32
    Width = 200
    Height = 21
    TabOrder = 0
    Text = 'Edit1'
  end
  object btnSalvar: TButton
    Left = 8
    Top = 110
    Width = 97
    Height = 25
    Caption = 'Salvar'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
  object Edit2: TEdit
    Left = 8
    Top = 80
    Width = 200
    Height = 21
    TabOrder = 2
    Text = 'Edit1'
  end
  object btnCancelar: TButton
    Left = 111
    Top = 110
    Width = 97
    Height = 25
    Caption = 'Cancelar'
    ModalResult = 2
    TabOrder = 3
  end
  object CheckBox1: TCheckBox
    Left = 214
    Top = 34
    Width = 97
    Height = 17
    Caption = 'Habilitado'
    TabOrder = 4
  end
end
