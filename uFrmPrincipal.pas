unit uFrmPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TForm12 = class(TForm)
    lblLblTeste: TLabel;
    lbl1: TLabel;
    lbl12: TLabel;
    lblXPTO: TLabel;
    pnlPrincipal: TPanel;
    Edit1: TEdit;
    btn_OV_732: TButton;
    Button1: TButton;
    btn_OV731: TButton;
    Label1: TLabel;
    procedure btn_OV_732Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure btn_OV731Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form12: TForm12;

implementation

{$R *.dfm}

uses U_FOV732,uFrmFerramenta, U_Fteste2;

procedure TForm12.btn_OV731Click(Sender: TObject);
begin
  pnl1.caption := 'Click btn_OV_731';
  application.processmessages;

  if F_Teste2 = nil then
     Application.CreateForm(TF_Teste2,F_Teste2);

  try
    F_Teste2.ShowModal;
  finally
    if Assigned(F_Teste2) then
      FreeAndNil(F_Teste2);
  end;
end;

procedure TForm12.btn_OV_732Click(Sender: TObject);
begin
  pnl1.caption := 'Click btn_OV_732';
  application.processmessages;

  if F_OV732 = nil then
     Application.CreateForm(TF_OV732,F_OV732);

  try
    F_OV732.ShowModal;
  finally
    if Assigned(F_OV732) then
      FreeAndNil(F_OV732);
  end;


end;



procedure TForm12.Button1Click(Sender: TObject);
var
  frmFerramenta: TfrmFerramenta;
begin
  pnl1.caption := 'Click Button1';
  application.processmessages;
  frmFerramenta := TfrmFerramenta.Create(Self);
  try
    if (frmFerramenta.ShowModal = mrOk) then
      MessageDlg('Salvo com sucesso.', mtInformation, [mbOK], 0)
    else
      MessageDlg('Opera��o cancelada.', mtWarning, [mbOK], 0);
  finally
    if Assigned(frmFerramenta) then
      FreeAndNil(frmFerramenta);
  end;

end;

end.
