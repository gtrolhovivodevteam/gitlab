program GitLab;

uses
  Vcl.Forms,
  uFrmPrincipal in 'uFrmPrincipal.pas' {Form12},
  uFrmFerramenta in 'uFrmFerramenta.pas' {frmFerramenta},
  U_FOV732 in 'U_FOV732.pas' {F_OV732},
  U_Fteste2 in 'U_Fteste2.pas' {F_Teste2};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm12, Form12);
  Application.CreateForm(TfrmFerramenta, frmFerramenta);
  Application.CreateForm(TF_OV732, F_OV732);
  Application.CreateForm(TF_Teste2, F_Teste2);
  Application.Run;
end.
